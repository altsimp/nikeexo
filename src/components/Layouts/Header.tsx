function Header() {
  return (
    <div className="w-screen min-h-screen flex flex-col flex-1 bg-zinc-700 justify-center items-center ">
      <div className=" relative flex flex-row p-6 bg-gradient-to-r  from-zinc-800  to-zinc-600 w-5/12 rounded-lg overflow-visible">
        <div className="w-1/2 flex align-middle">
          <img
            src="/Images/jordan1.png"
            alt="jordan1"
            className="origin-center rotate-45
             absolute -left-20 -top-10  max-w-[600px]"
          />
        </div>

        <div className="flex-col w-1/2">
          <h1 className="uppercase text-2xl font-bold bg-gradient-to-r from-red-500 to-red-100 text-transparent bg-clip-text">
            Nike
          </h1>
          <h2 className="uppercase text-3xl text-white ps-4"> Jordan</h2>

          <h3 className="uppercase text-xs font-bold text-white mt-10 mb-2">
            product overview
          </h3>

          <p className="text-slate-50 text-xs ps-8 leading-5">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora
            fugiat sequi consequatur illum et voluptas quae sint dignissimos
            voluptate est.
          </p>

          <div className="flex gap-2 ps-8 mt-6 text-xs text-white">
          <button className="w-7 h-7 rounded-md bg-zinc-600 hover:from-red-400 hover:to-red-500 hover:bg-gradient-to-r transition duration-300 shadow-md">
              7
            </button>
             <button className="w-7 h-7 rounded-md bg-zinc-600 hover:from-red-400 hover:to-red-500 hover:bg-gradient-to-r transition duration-300 shadow-md">
              8
            </button>
            <button className="w-7 h-7 rounded-md bg-zinc-600 hover:from-red-400 hover:to-red-500 hover:bg-gradient-to-r transition duration-300 shadow-md">
              9
            </button>
            <button className="w-7 h-7 rounded-md bg-zinc-600 hover:from-red-400 hover:to-red-500 hover:bg-gradient-to-r transition duration-300 shadow-md">
              10
            </button>
          </div>

          <div className="text-white font-bold flex gap-8 mt-10 items-center justify-end">
            <span className="text-shadow:-20px -10px 1px rgba(206,206,206,0.4)">
              $120
            </span>

            <button className="w-40 h-10 rounded-md bg-gradient-to-r from-red-500  to-red-500 hover:from-red-400 hover:to-red-300 hover:bg-gradient-to-r transition duration-300 shadow-md">
              POURCHASE
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
